# IDS721 Week4 Miniproject

## NetID: zs148

In this project, I have built a docker container to run a simple Rust web app. This web app will print welcome slogan.

## Screenshots of showing container running
main.rs code
![image](./images/main.png)

Dockerfile content
![image](./images/dockerfile.png)

Get hostname
![image](./images/hostname.png)

Build docker image
![image](./images/build%20image.png)

Run container locally
![image](./images/run%20image.png)

The running website
![image](./images/website%20image.png)

## Install Docker
Use .sh script provided by Docker offical website to automatically install Docker.
```
curl -fsSL https://test.docker.com -o test-docker.sh
sudo sh get-docker.sh
```

## Verify Docker
Use the following command to test whether Docker has been installed well on your system.
```
sudo docker run hello-world
```
If the terminal appeals this means that Docker has been well installed.
```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
b8dfde127a29: Pull complete
Digest: sha256:308866a43596e83578c7dfa15e27a73011bdd402185a84c5cd7f32a88b501a24
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

## Create a Rust web application
1. Create a template by
    ```
    cargo new actix_web_app
    ```
2. Add needed dependencies to `Cargo.toml`
3. Write content in the `src/main.rs`
4. Verify the application locally by `cargo run`

## Write Dockerfile
My Dockerfile:
```
# Use the right version correponding to your rust version
FROM rust:latest AS builder

# set up work directory
WORKDIR /myapp
USER root

# copy the entire project into the working dicrectory
COPY . .

# compile rust app in the working directory
RUN cargo build --release

# use the right image according to different versions of glibc
FROM debian:bookworm-slim

# set up working directory
WORKDIR /myapp

# copy the executable file to the working directory for easily launching
COPY --from=builder /myapp/target/release/actix_web_app /myapp

# expose port
EXPOSE 8080

# run the app
CMD ["./actix_web_app"]
```

## Build the Docker image and run container
### Build your Docker image by
```
sudo docker build -t <YOUR_IMAGE> .
```

### Run the container locally by
```
sudo docker run -p 8080:8080 <YOUR_IMAGE>
```
Then you can check your web application if running.

## To test my web application
After run the container locally, use the following command to get your hostname:
```
hostname -I
```
Then use the `<YOUR_IP_ADDRESS>:8080` as url to check the running website.

